import java.time.LocalDate;
import java.util.Date;

public class File {
    private Face concluded;
    private String target;
    private double sell;
    private LocalDate date;
    private Staff staff;

    public File(Face concluded, String target, double sell, LocalDate date, Staff staff) {
        this.concluded = concluded;
        this.target = target;
        this.sell = sell;
        this.date = date;
        this.staff = staff;
    }

    public double getSell() {
        return sell;
    }

    public LocalDate getDate() {
        return date;
    }

    public Face getConcluded() {
        return concluded;
    }

    public String getTarget() {
        return target;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setSell(double sell) {
        this.sell = sell;
    }

    public String toString() {
        return this.concluded + "\n"
                + this.target + "\n"
                + this.sell + "\t" + this.date + "\n"
                + this.staff.getFirstName() + " " + this.staff.getSurName();
    }
}

