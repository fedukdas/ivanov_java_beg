import java.time.LocalDate;
import java.util.Scanner;

public class Cons {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Создайте договор");
        System.out.println("Выберите какое лицо оформляет договор юр(1)|физ(2)");
        Face concluded = Face.IS_PHYSICAL;
        int choices = scanner.nextInt();
        if (choices == 1) {
            concluded = Face.IS_LEGAL;
        }
        System.out.println("Введите цель договора");
        String target = scanner.nextLine();
        System.out.println("Введите цену договора");
        double sell = scanner.nextDouble();
        System.out.println("Введите дату договора в формате дд-мм-гггг");
        LocalDate date = LocalDate.parse(scanner.nextLine());
        System.out.println("Введите ФИ сотрудника, через пробел");
        String fi = scanner.nextLine();
        System.out.println("Введите возраст");
        int age = scanner.nextInt();
        System.out.println("Введите должность");
        String position = scanner.nextLine();
        Staff staff = new Staff(fi.split(" ")[1], fi.split(" ")[0], age, position);
        File file = new File(concluded, target, sell, date, staff);
        System.out.println(file.toString());
    }
}
