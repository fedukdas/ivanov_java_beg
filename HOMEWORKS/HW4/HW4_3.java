import java.util.Scanner;

public class HW4_3 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
		
        while (true) {
            System.out.println("Введите обычное число. Комп чет нарисует.");
            int num = s.nextInt();
            int numIt = num;
			
            if (num > 0) {
                System.out.println("Вы ввели число" + " " + num);
				
                while (num > 0) {
                    for (int w = (num-1); w != 0; w--) {
                        System.out.print(" ");
                    }
					
                    for (int It = numIt; It > 0; It--) {
                        System.out.print("0");
                    }
					
                    System.out.println(" ");
                    num--;
                }
            } else {
                System.out.println("Ошибка! Сами думайте как исправить!");
            }
            System.out.println(" ");
        }
    }
}
