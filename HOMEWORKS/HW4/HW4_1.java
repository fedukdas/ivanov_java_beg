import java.util.Scanner;

public class HW4_1 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
		
        while (true) {
            System.out.println("Введите обычное число!");
            int number = s.nextInt();
			
            if (number > 0) {
                int chtoto = 10;
                int sum = 0;
				
                while (number % chtoto != number) {
                    sum = sum + number % chtoto * 10 / chtoto;
                    chtoto = chtoto * 10;
                }
				
                sum = sum + number % chtoto * 10 / chtoto;
				
                System.out.println(sum);
            } else {
                System.out.println("Ошибка! Сами думайте как исправить!");
				
                System.out.println(" ");
				
                continue;
            }
			
            System.out.println(" ");
        }
    }
}
