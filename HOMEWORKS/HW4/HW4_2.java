import java.util.Scanner;

public class HW4_2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
		 
        while (true) {
            System.out.println("Введите обычное число! Комп чет нарисует.");
            int num = s.nextInt();
            int numIt = num;
			
            if (num > 0) {
                System.out.println("Вы ввели" + " " + num);
				
                while (num > 0) {
                    for (int It = numIt; It > 0; It--)
                        System.out.print("*");
					
                    System.out.println(" ");
                    num--;
                }
				
            } else {
                System.out.println("Ошибка! Сами думайте как исправить!");
            }
            System.out.println(" ");
        }
    }
}
