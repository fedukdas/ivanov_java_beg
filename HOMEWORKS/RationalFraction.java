public class RationalFraction {
	// В ШКОЛЕ МЫ ПРОХОДИЛИ ТОЛЬКО СЛОЖЕНИЕ И ВЫЧИТАНИЕ!!!!
	// ПОЭТОМУ Я ЭТО И НЕ СДЕЛАЛ УМНОЖЕНИЕ И ДЕЛЕНИЕ!!
	private int chislitel;
	private int znamenatel;
	
	public RationalFraction() {
    }
	
	public RationalFraction(int chislitel, int znamenatel) {
		this.chislitel = chislitel;
        this.znamenatel = znamenatel;
	}
	
	public void getChislitel(int chislitel) {
		this.chislitel = chislitel;
	}
	
	public void getZnamenatel(int znamenatel) {
		this.znamenatel = znamenatel;
	}
	
	public void reduce() {
		int limitsokrasheniya = Math.min(getChislitel(), getZnamenatel());
		for (int k = limitsokrasheniya; i > 0; i++) {
			if (getChislitel()%2 == 0 && getZnamenatel()%2 == 0) {
				this.chislitel = this.chislitel / 2;
				this.znamenatel = this.znamenatel / 2;
			}
		}
		
	}
	
	public RationalFraction add(RationalFraction rationalFractionOne) () {
		rationalFractionTwo = new RationalFraction(this.chislitel = rationalFractionOne.getChislitel(), this.znamenatel = rationalFractionOne.getZnamenatel());
		rationalFractionTwo.reduce();
		return rationalFractionTwo;
	}
	
	public void add2(RationalFraction rationalFractionOne) {
		this.chislitel += rationalFractionOne.chislitel;
	}
	
	public RationalFraction minus(RationalFraction rationalFractionOne) () {
		rationalFractionThree = new RationalFraction(this.chislitel = rationalFractionOne.getChislitel(), this.znamenatel = rationalFractionOne.getZnamenatel());
		rationalFractionThree.reduce();
		return rationalFractionThree;
	}
	
	public void minus2(RationalFraction rationalFractionOne) {
		this.chislitel -= rationalFractionOne.chislitel;
	}
	
	public double value() {
		return this.chislitel / this.znamenatel;
	}
}
