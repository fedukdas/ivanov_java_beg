import java.util.Random;

public class Player {
    private int xp;
    private String nickName;

    public Player(String nickName) {
        this.xp = 100;
        this.nickName = nickName;
    }

    public int getXp() {
        return xp;
    }

    public String getnickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public void kicked(int power) {
        if ((power > 0) && (power < 10)) {
            Random r = new Random( );
            int ver = r.nextInt(100);
            if (power == 1) {
                this.xp -= power * 10;
            } else if ((power == 2) && (ver <= 89)) {
                this.xp -= power * 10;
            } else if ((power == 3) && (ver <= 78)) {
                this.xp -= power * 10;
            } else if ((power == 4) && (ver <= 67)) {
                this.xp -= power * 10;
            } else if ((power == 5) && (ver <= 56)) {
                this.xp -= power * 10;
            } else if ((power == 6) && (ver <= 45)) {
                this.xp -= power * 10;
            } else if ((power == 7) && (ver <= 34)) {
                this.xp -= power * 10;
            } else if ((power == 8) && (ver <= 23)) {
                this.xp -= power * 10;
            } else if ((power == 9) && (ver <= 12)) {
                this.xp -= power * 10;
            }
        }
    }

    public boolean isAlive() {
        return this.xp > 0;
    }

    public String toString() {
        return this.nickName + "\n" + this.xp;
    }
}
