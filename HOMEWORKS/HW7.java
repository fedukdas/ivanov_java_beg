import java.util.Scanner;

public class HW7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество элементов");
		
        int[] array = getArray(scanner.nextInt(), scanner);
        System.out.println(toString(array));
		System.out.println(toString(bubbleSort(array)));
        System.out.println(arrayplus(array));
        System.out.println(arrayMin(array));
        System.out.println(arrayMax(array));
    }
	
	public static int[] getArray(int leght, Scanner scanner) {
        int array[] = new int[leght];
        for (int i = 0; i < leght; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }
	
	public static String toString(int[] array) {
        String line = "";
        for (int i = 0; i < array.length; i++) {
            line = line + array[i] + " ";
        }
        line = line + "\b";
        return line;
    }
	
	public static int arraySum(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum = sum + array[i];
        }
        return sum;
    }
	
	public static int[] bubbleSort(int[] array) {
        int buffer;
        for (int i_vn = array.length; i_vn >= 0; i_vn--)
            for (int i = 0; i < i_vn - 1; i++) {
                if (array[i] > array[i + 1]) {
                    buffer = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = buffer;
                }
            }
        return array;
    }
	
    public static int arrayMax(int[] array) {
        return array[array.length - 1];
    }

    public static int arrayMin(int[] array) {
        return array[0];
    }
}
